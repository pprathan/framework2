package com.leafBot.pages;

import org.openqa.selenium.WebElement;

import com.leafBot.testng.api.base.Annotations;

public class CreateLeadPage extends Annotations {
	String id;
	
	public CreateLeadPage typeCompanyName(String cName) {
		driver.findElementById("createLeadForm_companyName")
		.sendKeys(cName);
		return this;
	}
	
	public CreateLeadPage typeFirstName(String fName) {
		driver.findElementById("createLeadForm_firstName")
		.sendKeys(fName);
		return this;
	}
	
	public CreateLeadPage typeLastName(String lName) {
		WebElement lastName = locateElement(id, "username");
		clearAndType(lastName, "PRathan");
		return this;
	}
	
	public ViewLeadPage clickCreateLeadButton() {
		driver.findElementByClassName("smallSubmit")
		.click();
		String companyName = driver.findElementById("viewLead_companyName_sp").getText();
		id = companyName.replaceAll("\\D", "");
		return new ViewLeadPage();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
