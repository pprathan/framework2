package com.leafBot.pages;

import com.leafBot.testng.api.base.Annotations;

public class ViewLeadPage extends Annotations {
	
	String id;
	
	public ViewLeadPage verifyFirstName() {
		String text = driver.findElementById("viewLead_firstName_sp")
		.getText();
		if(text.equals("Babu")) {
			System.out.println("First name matches with input data");
		}else {
			System.err.println("First name not matches with input data");
		}
		return this;
	}	
	
	public ViewLeadPage verifyLeadIsCreated() {
		String companyNameWithID = driver.findElementById("viewLead_companyName_sp").getText();
		String leadID = companyNameWithID.replaceAll("\\D", "");
		int leadIDinNumber = Integer.parseInt(leadID);
		
		if(leadIDinNumber > 0) {
			System.out.println("Lead -"+leadID+"is created");
		}
		else
			System.err.println("Lead is not created");
		
		return this;
		
	}

}
